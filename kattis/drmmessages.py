#!/usr/bin/env python

ciphertext=input()
characters='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
charlist=['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
if len(ciphertext)%2==0:
  partsize=int(len(ciphertext)/2)
  ciph=ciphertext[:partsize]
  key=ciphertext[partsize:]
else:
  print('nonvalid')
ciphval=0
keyval=0
for char in ciph:
  ciphval=ciphval+charlist.index(char)
for char in key:
  keyval=keyval+charlist.index(char)
ciphrot=''
keyrot=''
for char in ciph:
  newcharval=(ciphval+charlist.index(char))%26
  ciphrot+=charlist[newcharval]
for char in key:
  newcharval=(keyval+charlist.index(char))%26
  keyrot+=charlist[newcharval]
plaintext=''
for i in range(partsize):
  newcharval=(charlist.index(ciphrot[i])+charlist.index(keyrot[i]))%26
  plaintext+=charlist[newcharval]
print(plaintext)