#!/usr/bin/env python

nEvents=int(input())
daysFreeFood=[]
for i in range(nEvents):
  startEndEvent=[int(x) for x in input().split()]
  daysOfEvent=range(startEndEvent[0],startEndEvent[1]+1)
  for day in daysOfEvent:
    if not day in daysFreeFood:
      daysFreeFood.append(day)
print(len(daysFreeFood))